﻿// SkyProbe Fog by Silent
// version 3

// Based off Distance Fade Cube Volume
// by Neitri, free of charge, free to redistribute
// from https://github.com/netri/Neitri-Unity-Shaders

Shader "Silent/SkyProbe Fog (Horizon)"
{
    Properties
    {
        [Header(Base)]
        _Color("Color Tint", Color) = (1,1,1,1)
        [Space]
        [ToggleUI]_UseDepthColours("Use Near/Mid/Far Colours", Float) = 0 
        _Color1("Near Color Tint", Color) = (1,1,1,1)
        _Color1Start("Near Start", Float) = 0
        _Color2("Mid Color Tint", Color) = (0.5,0.5,0.5,1)
        _Color2Start("Mid Start", Float) = 500
        _Color3("Far Color Tint", Color) = (0,0,0,1)
        _Color3Start("Far Start", Float) = 1000

        [Header(Probe Settings)]
        [NoScaleOffset]_ProbeTexture("Probe Texture Override", Cube) = "unity_SpecCube0" {}
        _BlurLevelNear("Blur Level Near (def: 4)", Range(0, 7)) = 7
        _BlurLevelFar("Blur Level Far (def: 4)", Range(0, 7)) = 4
        _ProbeAlpha("Probe Alpha", Range(0, 1)) = 1

        [Header(Fog)]
        _FogStrength ("Fog Strength", Float) = 1.0
        _FadeFalloff ("Fade Falloff", Float) = 1.0
        [Space]
        [ToggleUI]_ApplyClip ("Don't apply to foreground", Float) = 0.0
        [ToggleUI]_ApplySkybox ("Don't apply to skybox", Float) = 0.0
        [Space]
        [ToggleUI]_ApplySun ("Show Sun", Float) = 0.0
        _SunSize ("Sun Size", Range(0, 1)) = 0.04

        [Header(System)]
        [Enum(UnityEngine.Rendering.CullMode)] _CullMode("Cull Mode", Float) = 0

        [Enum(UnityEngine.Rendering.BlendMode)]
        _SrcBlend("Src Factor", Float) = 5  // SrcAlpha
        [Enum(UnityEngine.Rendering.BlendMode)]
        _DstBlend("Dst Factor", Float) = 10 // OneMinusSrcAlpha
        [Space]
        [Toggle(BLOOM)]_NoPremultiplyAlpha("Don't premultiply alpha", Float) = 0.0
    }
    SubShader
    {
        Tags
        {
            "Queue" = "Transparent-216"
            "RenderType" = "Custom"
            "ForceNoShadowCasting"="True" 
            "IgnoreProjector"="True"
            "DisableBatching"="True"
        }

        ZWrite Off
        ZTest Always
        Cull[_CullMode]
        Blend[_SrcBlend][_DstBlend]

        Pass
        {
            Lighting On
            Tags
            {
                "LightMode" = "Always"
            }
            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0

            #pragma multi_compile_instancing
            // #pragma multi_compile_fwdbase nolightmap nodynlightmap novertexlight
            // #pragma multi_compile_fwdadd_fullshadows

            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"

            #if defined(SHADER_API_D3D11) || defined(SHADER_API_XBOXONE) || defined(UNITY_COMPILER_HLSLCC)//ASE Sampler Macros
            #define UNITY_SAMPLE_TEX2D_LOD(tex,coord,lod) tex.SampleLevel (sampler##tex,coord, lod)
            #else
            #define UNITY_SAMPLE_TEX2D_LOD(tex,coord,lod) tex2Dlod(tex,float4(coord,0,lod))
            #endif

            #pragma shader_feature _ BLOOM

            #define _PREMULTIPLY defined(BLOOM)

            float4 _Color;
            float4 _LightScaleOffset;
            float _ProbeAlpha;
            float _FadeFalloff;
            float _ApplyClip;
            float _ApplySkybox;
            float _ApplySun;
            float _FogStrength;
            float _BlurLevel;
            float _SunSize;
            float _BlurLevelNear;
            float _BlurLevelFar;
            float _FogVerticalOffset;
            int _FadeType;

            float _UseDepthColours;
            float4 _Color1;
            float4 _Color2;
            float4 _Color3;
            float _Color1Start;
            float _Color2Start;
            float _Color3Start;

            UNITY_DECLARE_TEXCUBE(_ProbeTexture); float4 _ProbeTexture_HDR;

            struct appdata
            {
                UNITY_VERTEX_INPUT_INSTANCE_ID 
                float4 vertex : POSITION;
            };

            struct v2f
            {
                UNITY_VERTEX_INPUT_INSTANCE_ID 
                UNITY_VERTEX_OUTPUT_STEREO
                float4 pos : SV_POSITION;
                float4 depthTextureUv : TEXCOORD1;
                float4 rayFromCamera : TEXCOORD2;
                float4 worldPosition : TEXCOORD4;
                SHADOW_COORDS(3)
            };

            UNITY_DECLARE_DEPTH_TEXTURE(_CameraDepthTexture);

            // normalizedcrow's method for getting linear
            float GetLinearDepth(float4 clipPos)
            {
                float4 screenPos = ComputeScreenPos(clipPos);
                float2 screenUV = screenPos.xy / screenPos.w;
                float2 normalizedDeviceCoordinates = clipPos.xy / clipPos.w;

                //sample depth texture
                float projectedDepth = SAMPLE_DEPTH_TEXTURE_LOD(_CameraDepthTexture, float4(screenUV, 0.0, 0.0));

                //handle skewed depth planes (such as in mirrors), works for both orthographic and perspective projections
                //(in VR the eye projection matrix can also skew the clip pos based on depth which is why the _m02 and _m12 terms
                //are here. that part isn't correct for orthographic projections but those terms should always be 0 then anyway)
                projectedDepth -= ((normalizedDeviceCoordinates.x + UNITY_MATRIX_P._m02) / UNITY_MATRIX_P._m00) * UNITY_MATRIX_P._m20;
                projectedDepth -= ((normalizedDeviceCoordinates.y + UNITY_MATRIX_P._m12) / UNITY_MATRIX_P._m11) * UNITY_MATRIX_P._m21;

                //un-project depth
                float depth = (UNITY_MATRIX_P._m33 == 0.0) //check if this is a perspective matrix
                            ? (UNITY_MATRIX_P._m23 / (projectedDepth + UNITY_MATRIX_P._m22)) //perspective
                            : ((UNITY_MATRIX_P._m23 - projectedDepth) / UNITY_MATRIX_P._m22); //orthographic
                
                return depth;
            }

            // Dj Lukis.LT's oblique view frustum correction (VRChat mirrors use such view frustum)
            // https://github.com/lukis101/VRCUnityStuffs/blob/master/Shaders/DJL/Overlays/WorldPosOblique.shader
            inline float4 CalculateObliqueFrustumCorrection()
            {
                float x1 = -UNITY_MATRIX_P._31 / (UNITY_MATRIX_P._11 * UNITY_MATRIX_P._34);
                float x2 = -UNITY_MATRIX_P._32 / (UNITY_MATRIX_P._22 * UNITY_MATRIX_P._34);
                return float4(x1, x2, 0, UNITY_MATRIX_P._33 / UNITY_MATRIX_P._34 + x1 * UNITY_MATRIX_P._13 + x2 * UNITY_MATRIX_P._23);
            }
            inline float CorrectedLinearEyeDepth(float z, float correctionFactor)
            {
                return 1.f / (z / UNITY_MATRIX_P._34 + correctionFactor);
            }

            bool SceneZDefaultValue()
            {
                #if UNITY_REVERSED_Z
                    return 0.f;
                #else
                    return 1.f;
                #endif
            }

            v2f vert(appdata v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_OUTPUT(v2f, o); 
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                float4 worldPosition = mul(unity_ObjectToWorld, v.vertex);
                const fixed3 baseWorldPos = unity_ObjectToWorld._m03_m13_m23;
                o.pos = mul(UNITY_MATRIX_VP, worldPosition);
                o.depthTextureUv = ComputeGrabScreenPos(o.pos);
                // Warp ray by the base world position, so it's possible to have reoriented fog
                o.rayFromCamera.xyz = (worldPosition.xyz - _WorldSpaceCameraPos.xyz);
                o.rayFromCamera.w = dot(o.pos, CalculateObliqueFrustumCorrection()); // oblique frustrum correction factor
                o.worldPosition = worldPosition;
                //o.vertex2 = float4(UnityObjectToViewPos(v.pos), 1.0);
                TRANSFER_SHADOW(o);
                UNITY_TRANSFER_INSTANCE_ID(v, o);
                return o;
            }

            float fogFactorExp2( float dist, float density) 
            {
              const float LOG2 = -1.442695;
              float d = density * dist;
              return 1.0 - clamp(exp2(d * d * LOG2), 0.0, 1.0);
            }

            // Calculates the sun shape
            half calcSunAttenuation(half3 lightPos, half3 ray)
            {
                half3 delta = lightPos - ray;
                half dist = length(delta);
                half sunSize =  _SunSize; 
                half spot = 1.0 - smoothstep(0.0, sunSize, dist);
                return spot * spot;
            }

            float3 worldPosFromDepth(sampler2D depthTexture, float2 depthSamplePos, float3 camPos, float4 worldCoordinates)
            {
                float sampleDepth = SAMPLE_DEPTH_TEXTURE(depthTexture, depthSamplePos);
                sampleDepth = DECODE_EYEDEPTH(sampleDepth);

                // https://gamedev.stackexchange.com/questions/131978/shader-reconstructing-position-from-depth-in-vr-through-projection-matrix
                float3 viewDirection = (worldCoordinates.xyz - camPos) / (-mul(UNITY_MATRIX_V, worldCoordinates).z);

                return camPos + viewDirection * sampleDepth;
            }

            float4 frag(v2f i) : SV_Target
            {
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i); 
                float perspectiveDivide = 1.f / i.pos.w;
                float4 rayFromCamera = i.rayFromCamera * perspectiveDivide;

                // https://gamedev.stackexchange.com/questions/131978/shader-reconstructing-position-from-depth-in-vr-through-projection-matrix
                rayFromCamera.xyz = (i.worldPosition.xyz - _WorldSpaceCameraPos) / (-mul(UNITY_MATRIX_V, i.worldPosition).z);

                float2 depthTextureUv = i.depthTextureUv.xy * perspectiveDivide;
                const fixed3 baseWorldPos = unity_ObjectToWorld._m03_m13_m23;

                float sceneZ = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, depthTextureUv);
                bool isSkybox = (sceneZ == SceneZDefaultValue());
                if (isSkybox)
                {
                    // This is skybox, depth texture has default value
                    // It's scene dependant on whether we want the fog to be clipped by the skybox
                    clip(-_ApplySkybox);
                    sceneZ = 0;
                }

                // linearize depth and use it to calculate background world position
                float sceneDepth = CorrectedLinearEyeDepth(sceneZ, rayFromCamera.w*perspectiveDivide);
                float3 worldPosition = rayFromCamera.xyz * sceneDepth + _WorldSpaceCameraPos.xyz;
                float4 localPosition = mul(unity_WorldToObject, float4(worldPosition, 1));
                float localDepth = CorrectedLinearEyeDepth(i.pos.z, rayFromCamera.w);

                worldPosition = worldPosFromDepth(_CameraDepthTexture, depthTextureUv, _WorldSpaceCameraPos, i.worldPosition);

                float clipAt = (localDepth < sceneDepth);

                localPosition.xyz /= localPosition.w;
                
                float fade = max(0, 0.5 - localPosition.z) * 1/_FadeFalloff;
                fade = min(fade, 1.0);

                float fogDepthForBlur = saturate(exp(-sceneDepth*_FadeFalloff));
                float blurLevel = lerp(_BlurLevelFar, _BlurLevelNear, fogDepthForBlur);

                float4 color = UNITY_SAMPLE_TEXCUBE_LOD( _ProbeTexture, rayFromCamera, blurLevel);
                color = float4(DecodeHDR(color, _ProbeTexture_HDR), 1.0);

                color = lerp(1, color, _ProbeAlpha);

                if (_UseDepthColours)
                {
                    float fogDistance = (length(rayFromCamera.xyz * sceneDepth));
                    float fogRange1 = saturate((fogDistance-_Color1Start)/_Color2Start);
                    float fogRange2 = saturate((fogDistance-_Color2Start)/_Color3Start);
                    color *= lerp(lerp(_Color1, _Color2, fogRange1), _Color3, fogRange2);
                }
                else 
                {
                    color *= _Color;
                }

                // Add sun
                if (_ApplySun) 
                {
                    float sunAtt = calcSunAttenuation(_WorldSpaceLightPos0.xyz, rayFromCamera);
                    //color.rgb = color.rgb * (1-sunAtt) + sunAtt * _LightColor0.xyz;
                    color.rgb += sunAtt * _LightColor0.xyz;
                }

                fade = pow(1-abs(rayFromCamera.y), 1/(0.2*_FogStrength));
                fade *= fogFactorExp2(_FogStrength*0.02, sceneDepth);

                #if 0 // Old blending, kept for reference.
                color.a *= fogAmount;
                color.a = saturate(color.a);
                #else
                color.a *= saturate(fade);
                #endif

                if (_ApplyClip) color.a *= clipAt;

                #if !_PREMULTIPLY
                color.rgb *= color.a;
                #endif

                return color;
            }

            ENDCG
        }
    }
    }